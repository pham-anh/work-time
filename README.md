# Work Time

* Version: 1.0.0
* [Development page](https://gitlab.com/pham-anh/work-time)

## Description

Work Time is an web application written in [FuelPHP](https://fuelphp.com). 

Its purpose is to help manage work time in your company such as leaves and overtime.

## Requirements

See [requirements of FuelPHP Framework](https://fuelphp.com/docs/requirements.html)

## Install

* Clone or download the project into your web root

```shell
git clone https://gitlab.com/pham-anh/work-time.git
```

* Update composer

```shell
php composer.phar self-update
```

* Run composer to install the dependencies

```shell
php composer.phar install
```



* Create a database for the system
* Set database information in `fuel/app/config/production/db.php`. You need to set the following information:

```php
<?php
return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=fuel_prod', // replace <fuel_prod> by your database name
			'username'   => 'fuel_app', // replace <fuel_app> by your database's username
			'password'   => 'super_secret_password', // replace <super_secret_password> by the password of your database's username
		),
	),
);
```

* Set up database by the following command
 
```shell
env FUEL_ENV=production php oil r migrate --packages=auth
env FUEL_ENV=production php oil r migrate
```

