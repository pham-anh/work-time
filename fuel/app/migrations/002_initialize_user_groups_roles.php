<?php

namespace Fuel\Migrations;

/**
 * Initialize groups, roles, admin user
 *
 * @since 1.0.0
 * @author Pham Quynh Anh
 */
class Initialize_user_groups_roles
{

	/**
	 * Add groups: locked and co-workers
	 *     roles: locked, co-worker, hr and administrator
	 *     user: admin
	 *
	 * @param void
	 * @return void
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function up()
	{
		$table = \Config::get('ormauth.table_name', 'users');

		// Initialize groups
		list($locked_group_id, $rows_affected) = \DB::insert($table.'_groups')->set(array(
			'name' => 'locked',
			'created_at' => time()
		))->execute();

		list($coworker_group_id, $rows_affected) = \DB::insert($table.'_groups')->set(array(
			'name' => 'co-workers',
			'created_at' => time()
		))->execute();

		// Initialize roles
		list($locked_role_id, $rows_affected) = \DB::insert($table.'_roles')->set(array(
			'name' => 'locked',
			'filter' => 'D',
			'created_at' => time()
		))->execute();

		list($coworker_role_id, $rows_affected) = \DB::insert($table.'_roles')->set(array(
			'name' => 'co-worker',
			'created_at' => time()
		))->execute();

		\DB::insert($table.'_roles')->set(array(
			'name' => 'hr',
			'created_at' => time()
		))->execute();

		list($admin_role_id, $rows_affected) = \DB::insert($table.'_roles')->set(array(
			'name' => 'administrator',
			'filter' => 'A',
			'created_at' => time()
		))->execute();
		
		// assign roles for groups
		$locked_role    = \Model\Auth_Role::find($locked_role_id);
		$coworker_role  = \Model\Auth_Role::find($coworker_role_id);
		$admin_role     = \Model\Auth_Role::find($admin_role_id);

		$locked_group = \Model\Auth_Group::find($locked_group_id);
		$locked_group->roles[] = $locked_role;
		$locked_group->save();
		
		$coworker_group = \Model\Auth_Group::find($coworker_group_id);
		$coworker_group->roles[] = $coworker_role;
		$coworker_group->save();

		// create admin user, give it all permissions
		$admin_id = \Auth::instance()
			->create_user('admin', 'admin', 'admin@example.com', $coworker_group_id, array('since' => time()));
		$admin = \Model\Auth_User::find($admin_id);
		$admin->roles[] = $admin_role;
		$admin->save();
	}

	/**
	 * Remove groups, roles users
	 *
	 * @param void
	 * @return void
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function down()
	{
		$table = \Config::get('ormauth.table_name', 'users');

		\DBUtil::truncate_table($table.'_group_roles');
		\DBUtil::truncate_table($table.'_groups');
		\DBUtil::truncate_table($table.'_user_roles');
		\DBUtil::truncate_table($table.'_roles');
		\DBUtil::truncate_table($table.'_metadata');
		\DBUtil::truncate_table($table);
	}

}