<?php

namespace Fuel\Migrations;

/**
 * add_user_management_permission
 *
 * @since 1.0.0
 * @author Pham Quynh Anh
 */
class add_user_management_permission
{

	/**
	 * up
	 *
	 * @param void
	 * @return void
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function up()
	{
		try
		{
			\DB::start_transaction();

			// initialize permission
			$permission = \Model\Auth_Permission::forge(array(
				'area'        => 'app',
				'permission'  => 'user',
				'description' => 'User management',
				'actions'     => array('login', 'logout', 'register', 'list', 'update', 'delete'),
				'created_at'  => time(),
			));
			$permission->save();

			$user_actions = array_flip($permission->actions);
			// assign login, logout permission to co-workers role
			$coworker_role = \Model\Auth_Role::find_by_name('co-worker');
			$coworker_role->rolepermission[] = \Model\Auth_Rolepermission::forge(array(
				'role_id'  => $coworker_role->id,
				'perms_id' => $permission->id,
				'actions'  => array($user_actions['login'], $user_actions['logout']),
			));
			$coworker_role->save();

			\DB::commit_transaction();
		}
		catch (Fuel\Core\PhpErrorException $e)
		{
			\DB::rollback_transaction();
			throw $e;
		}
	}

	/**
	 * down
	 *
	 * @param void
	 * @return void
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function down()
	{
		try
		{
			\DB::start_transaction();
			$coworker_role   = \Model\Auth_Role::find_by_name('co-worker');
			$user_permission = \Model\Auth_Permission::find_by_permission('user');

			unset($coworker_role->rolepermission[$user_permission->id]);
			$coworker_role->save();
			$user_permission->delete();

			\DB::commit_transaction();
		}
		catch (Fuel\Core\PhpErrorException $e)
		{
			\DB::rollback_transaction();
			throw $e;
		}
	}

}