<?php

namespace Fuel\Migrations;

/**
 * Truncate sample data which was added when running `php oil r migrate --packages=auth`
 *
 * When we run auth package migrations, the tables for the package were added
 * Along with that, the sample data was added into DB also
 * So we need to remove the unnessary sample data
 * 
 * @since 1.0.0
 * @author Pham Quynh Anh
 */
class Truncate_sample_data_of_auth_package
{

	/**
	 * Truncate sample data
	 *
	 * @param void
	 * @return void
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function up()
	{
		$table = \Config::get('ormauth.table_name', 'users');

		\DBUtil::truncate_table($table.'_group_roles');
		\DBUtil::truncate_table($table.'_groups');
		\DBUtil::truncate_table($table.'_roles');
		\DBUtil::truncate_table($table.'_metadata');
		\DBUtil::truncate_table($table);
	}

	/**
	 * Return sample data of auth package
	 *
	 * @param void
	 * @return false
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function down()
	{
		$table = \Config::get('ormauth.table_name', 'users');

		/*
		 * This code was copied from migration of auth package: 005_auth_create_authdefaults.php
		 */

		// create the 'Banned' group, 'banned' role
		list($group_id, $rows_affected) = \DB::insert($table.'_groups')->set(array('name' => 'Banned'))->execute();
		list($role_id, $rows_affected) = \DB::insert($table.'_roles')->set(array('name' => 'banned', 'filter' => 'D'))->execute();
		\DB::insert($table.'_group_roles')->set(array('group_id' => $group_id, 'role_id' => $role_id))->execute();

		// create the 'Guests' group
		list($group_id_guest, $rows_affected) = \DB::insert($table.'_groups')->set(array('name' => 'Guests'))->execute();
		list($role_id_guest, $rows_affected) = \DB::insert($table.'_roles')->set(array('name' => 'public'))->execute();
		\DB::insert($table.'_group_roles')->set(array('group_id' => $group_id_guest, 'role_id' => $role_id_guest))->execute();

		// create the 'Users' group
		list($group_id, $rows_affected) = \DB::insert($table.'_groups')->set(array('name' => 'Users'))->execute();
		list($role_id_user, $rows_affected) = \DB::insert($table.'_roles')->set(array('name' => 'user'))->execute();
		\DB::insert($table.'_group_roles')->set(array('group_id' => $group_id, 'role_id' => $role_id_user))->execute();

		// create the 'Moderators' group
		list($group_id, $rows_affected) = \DB::insert($table.'_groups')->set(array('name' => 'Moderators'))->execute();
		list($role_id_mod, $rows_affected) = \DB::insert($table.'_roles')->set(array('name' => 'moderator'))->execute();
		\DB::insert($table.'_group_roles')->set(array('group_id' => $group_id, 'role_id' => $role_id_user))->execute();
		\DB::insert($table.'_group_roles')->set(array('group_id' => $group_id, 'role_id' => $role_id_mod))->execute();

		// create the 'Administrators' group
		list($group_id, $rows_affected) = \DB::insert($table.'_groups')->set(array('name' => 'Administrators'))->execute();
		list($role_id, $rows_affected) = \DB::insert($table.'_roles')->set(array('name' => 'administrator'))->execute();
		\DB::insert($table.'_group_roles')->set(array('group_id' => $group_id, 'role_id' => $role_id_user))->execute();
		\DB::insert($table.'_group_roles')->set(array('group_id' => $group_id, 'role_id' => $role_id_mod))->execute();
		\DB::insert($table.'_group_roles')->set(array('group_id' => $group_id, 'role_id' => $role_id))->execute();

		// create the 'Superadmins' group
		list($group_id_admin, $rows_affected) = \DB::insert($table.'_groups')->set(array('name' => 'Super Admins'))->execute();
		list($role_id_admin, $rows_affected) = \DB::insert($table.'_roles')->set(array('name' => 'superadmin', 'filter' => 'A'))->execute();
		\DB::insert($table.'_group_roles')->set(array('group_id' => $group_id_admin, 'role_id' => $role_id_admin))->execute();

		/*
		 * create the default admin user, so we have initial access
		 */

		// create the administrator account if needed, and assign it the superadmin group so it has all access
		$result = \DB::select('id')->from($table)->where('username', '=', 'admin')->execute();
		if (count($result) == 0)
		{
			\Auth::instance()->create_user('admin', 'admin', 'admin@example.org', $group_id_admin, array('fullname' => 'System administrator'));
		}

		// create the guest account
		list($guest_id, $affected) = \DB::insert($table)->set(
				array(
					'username' => 'guest',
					'password' => 'YOU CAN NOT USE THIS TO LOGIN',
					'email' => '',
					'group_id' => $group_id_guest,
					'last_login' => 0,
					'previous_login' => 0,
					'login_hash' => '',
					'user_id' => 0,
					'created_at' => time(),
					'updated_at' => 0,
				)
			)->execute();

		// adjust the id's, auto_increment doesn't want to create a key with value 0
		\DB::update($table)->set(array('id' => 0))->where('id', '=', $guest_id)->execute();

		// add guests full name to the metadata
		\DB::insert($table.'_metadata')->set(
			array(
				'parent_id' => 0,
				'key' => 'fullname',
				'value' => 'Guest',
			)
		)->execute();
	}
}