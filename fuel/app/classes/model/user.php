<?php

use Orm\Model;

/**
 * Model_User
 *
 * @since 1.0.0
 * @author Pham Quynh Anh
 */
class Model_User extends Model
{
	protected static $_properties = array(
		'id',
		'username',
		'password',
		'group_id',
		'email',
		'last_login',
		'previous_login',
		'login_hash',
		'user_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	/**
	 * Validate user
	 *
	 * @param string $factory type of validatation
	 * @return object Validation object
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public static function validate($factory)
	{
		$val = Validation::forge($factory);

		if ($factory == 'login')
		{
			self::validate_login($val);
			return $val;
		}

		if ($factory == 'register')
		{
			self::validate_register($val);
		}
		else
		{
			self::validate_update($val);
		}

		self::set_validation_messages($val);
		return $val;
	}

	/**
	 * Validate login form
	 *
	 * @param object $val Validation object
	 * @return object Validation object
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	protected static function validate_login($val)
	{
		$val->add('username', 'Username')
				->add_rule('required');
		$val->add('password', 'Password')
			->add_rule('required');
	}

	/**
	 * Validate registration form
	 *
	 * @param object $val Validation object
	 * @return object Validation object
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	protected static function validate_register($val)
	{
		$val->add_callable('appvalidation');

		$val->add('username', 'Username')
			->add_rule('required')
			->add_rule('unique', 'users.username')
			->add_rule('min_length', 5)
			->add_rule('max_length', 50)
			->add_rule('match_pattern', '/^[a-z]+/', 'to begin with a lower case alphabet')
			->add_rule('match_pattern', '/^[a-z_0-9]*$/', 'to contain only lower case alphabet, underscore and number');

		$val->add('password', 'Password')
			->add_rule('required')
			->add_rule('min_length', 8)
			->add_rule('max_length', 50)
			->add_rule('match_pattern', '/[A-Z]+/', 'to contain at least 1 upper case alphabet')
			->add_rule('match_pattern', '/[\W]+/', 'to contain at least 1 symbol (specical character)');

		$val->add('since', 'Join date')
			->add_rule('required')
			->add_rule('valid_date', 'Y-m-d');
	}

	/**
	 * Validate update form
	 *
	 * @param object $val Validation object
	 * @return object Validation object
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	protected static function validate_update($val)
	{
		$val->add('full_name', 'Full name')
			->add_rule('required')
			->add_rule('max_length', 50);


		$val->add('birthday', 'Birthday')
			->add_rule('valid_date', 'Y-m-d');

		$val->add('phone_number', 'Phone number')
			->add_rule('valid_string', array('numeric', 'spaces'));

		$val->add('email_address', 'Email address')
			->add_rule('valid_email');

		$val->add('current_address', 'Current address');

		$val->add('relationship', 'Relationship');

		$val->add('contact_name', "Contact person's name")
			->add_rule('required_with', 'urgency_number');

		$val->add('urgency_number', 'Phone number')
			->add_rule('required_with', 'relationship')
			->add_rule('required_with', 'contact_name')
			->add_rule('valid_string', array('numeric', 'spaces'));
	}

	/**
	 * Set validation messages
	 *
	 * @param object $val Validation object
	 * @return object Validation object
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	protected static function set_validation_messages($val)
	{
		$val->set_message('required', ':label is required.')
			->set_message('unique', ':label is existing.')
			->set_message('min_length', ':label is too short. Please use at least :param:1 characters.')
			->set_message('max_length', ':label is too long. Please use up to :param:1 characters.')
			->set_message('match_pattern', ':label is required :param:2.')
			->set_message('valid_string', ':label is expected to contain numbers and spaces only.')
			->set_message('valid_email', ':label is not valid.');
	}

	/**
	 * Upload user avatar
	 * (Refs: https://fuelphp.com/docs/classes/upload/usage.html#/usage_example)
	 *
	 * @param string $username The username that own the avatar
	 * @return array An array of information of the uploaded files
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public static function upload_avatar()
	{
		// Custom configuration for this upload
		$config = array(
			'path' => DOCROOT.'assets/img',
			'randomize' => true,
			'max_size' => '200000',
			'ext_whitelist' => array('jpg', 'jpeg', 'gif', 'png'),
		);

		// process the uploaded files in $_FILES
		Upload::process($config);

		// if there are any valid files
		if (Upload::is_valid())
		{
			// save them according to the config
			Upload::save();
		}

		return Upload::get_files(0);
	}

}
