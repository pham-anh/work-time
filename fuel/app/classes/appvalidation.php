<?php

/**
 * Extended validation rules needed for the app
 *
 *
 * @since 1.0.0
 * @author Pham Quynh Anh
 */
class AppValidation
{

	/**
	 * Validate if a field is unique
	 * Copied from https://fuelphp.com/docs/classes/validation/validation.html#/extending_validation
	 *
	 * @param object $val Validation instance
	 * @return string $options The string to be validated
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	 public static function _validation_unique($val, $options)
    {
        list($table, $field) = explode('.', $options);

        $result = DB::select(DB::expr("LOWER (\"$field\")"))
        ->where($field, '=', Str::lower($val))
        ->from($table)->execute();

        return ! ($result->count() > 0);
    }

}