<?php

/**
 * Common process for other controllers
 *
 * @since 1.0.0
 * @author Pham Quynh Anh
 */
class Controller_Common extends Controller_Template
{
	/**
	 * Check has access to the requested action
	 *
	 * @param void
	 * @return bool
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function before()
	{
		parent::before();

		$permission = Request::active()->route->controller_path;
		$action     = Request::active()->route->action;

		if ( ! in_array($action, array('login', 'logout')))
		{
			if ( ! Auth::check())
			{
				Session::set_flash('error', 'Please sign in.');
				Response::redirect('login');
			}

			if ( ! Auth::has_access('app.'.$permission.'['.$action.']'))
			{
				Session::set_flash('error', 'Access denied.');
				Response::redirect('logout'); // TODO: change to another page. Expected to see the error flash in that another page
			}
		}
	}

	/**
	 * Log user in
	 *
	 * @param void
	 * @return void
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function action_login()
	{
		// already logged in?
		Auth::check() and Response::redirect_back('user');

		// was the login form posted?
		if (Input::method() == 'POST')
		{
			$val = Model_User::validate('login');
			if (Security::check_token() and $val->run())
			{
				// check the credentials.
				Auth::instance()
					->login($val->validated('username'), $val->validated('password'))
				and Response::redirect_back('user');
			}

			Session::set_flash('error', 'Invalid username or password.');
		}

		$this->template->title = 'Login';
		$this->template->content = View::forge('common/login');
	}

	/**
	 * Log user out
	 *
	 * @param void
	 * @return void
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function action_logout()
	{
		Auth::logout();
		Response::redirect('login');
	}
}