<?php

/**
 * Controller_User
 *
 * @since 1.0.0
 * @author Pham Quynh Anh
 */
class Controller_User extends Controller_Common
{

	public function action_index()
	{
		$data['users'] = Model_User::find('all');
		$this->template->title = "Users";
		$this->template->content = View::forge('user/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('user');

		if ( ! $data['user'] = Model_User::find($id))
		{
			Session::set_flash('error', 'Could not find user #'.$id);
			Response::redirect('user');
		}

		$this->template->title = "User";
		$this->template->content = View::forge('user/view', $data);

	}

	/**
	 * Validate and register user
	 *
	 * @param void
	 * @return void
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function action_register()
	{
		if (Input::method() == 'POST')
		{
			if (! \Security::check_token())
			{
				Session::set_flash('error', 'Security token verification failed.');
				Response::redirect('user');
			}

			$val = Model_User::validate('register');
			if ($val->run())
			{
				$user_id = Auth::create_user(
					$val->validated('username'), $val->validated('password'), $val->validated('username').'@example.com', 2,
					array('since' => strtotime($val->validated('since')))
				);
				$user_id !== false ?
					Session::set_flash('success', 'Added user ['.$val->validated('username').']') :
					Session::set_flash('error', 'Could not save user.');
				Response::redirect('user');
			}

			foreach ($val->error() as $error)
			{
				Session::set_flash($error->field->name, $error->get_message());
			}
		}

		$this->template->title = "Co-workers";
		$this->template->content = View::forge('user/register');

	}

	/**
	 * Update user
	 *
	 * @param void
	 * @return void
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function action_update($id = null)
	{
		is_null($id) and Response::redirect('user');

		if ( ! $user = Model\Auth_User::find($id))
		{
			Session::set_flash('error', 'Could not find user.');
			Response::redirect('user');
		}

		if (Input::post())
		{
			if ( ! \Security::check_token())
			{
				Session::set_flash('error', 'Security token verification failed.');
				Response::redirect('user');
			}

			$val = Model_User::validate('update');
			if ($val->run())
			{
				$updated = Auth::update_user(
					array(
						'full_name'       => $val->validated('full_name'),
						'birthday'        => strtotime($val->validated('birthday')),
						'phone_number'    => $val->validated('phone_number'),
						'email_address'   => $val->validated('email_address'),
						'current_address' => $val->validated('current_address'),
						'relationship'    => $val->validated('relationship'),
						'contact_name'    => $val->validated('contact_name'),
						'urgency_number'  => $val->validated('urgency_number'),
					),
					$user->username
				);

				$updated !== false ?
					Session::set_flash('success', 'Updated user ['.$user->full_name.']') :
					Session::set_flash('error', 'Could not save user.');
				Response::redirect('user');
			}

			foreach ($val->error() as $error)
			{
				Session::set_flash($error->field->name, $error->get_message());
			}
		}

		$this->template->set_global('user', $user, false);
		$this->template->title   = "Co-workers";
		$this->template->content = View::forge('user/update');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('user');

		if ($user = Model_User::find($id))
		{
			$user->delete();

			Session::set_flash('success', 'Deleted user #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete user #'.$id);
		}

		Response::redirect('user');

	}

}
