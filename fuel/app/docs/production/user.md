# Co-workers

## Register co-workers

### Rules

* Username: 
 * Required, unique, length: 5 ~ 50 characters
 * Begin with a lower case alphabet
 * Contains only lower case alphabet, underscore and number

* Password:
 * Required, length: 8 ~ 50 characters
 * Contain:
     * at least 1 UPPER CASE alphabet, 
     * at least 1 symbol (special character)

* Full name:
 * Required, length: up to 50 characters

* Co-worker since:
 * Required
 * Date format