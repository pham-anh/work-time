<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<span class="navbar-brand">Work-time</span>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a role='presentation'  title="Version"><span class="glyphicon glyphicon glyphicon-tag" aria-hidden="true"></span> 1.0.0</a></li>
				<li><a>Follow your company working time</a></li>

			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container -->
</nav>
