<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a title="Follow your company working time" class="navbar-brand" href="#">Work-time</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Leave Request <span class="badge">42</span> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Pending list/ List</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Register</a></li>
						<li><a href="#">Your requests</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Rules for leaves</a></li>
						<li><a href="#">Recomendation leaves</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-hourglass" aria-hidden="true"></span> Overtime <span class="badge">42</span> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Pending list/ List</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Register</a></li>
						<li><a href="#">Your overtime</a></li>
					</ul>
				</li>
				<li><a href="#"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Calendar <span title="Incoming" class="badge">42</span> </a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Advanced <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Co-worker</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Work-time report</a></li>
					</ul>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<?php
					$email = "identicon";
					$size = 30;
					$grav_url = "https://www.gravatar.com/avatar/".md5(strtolower(trim($email)))."?d=identicon"."&s=".$size;
					?>
					<a href="#" class="avatar-padding dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo Html::img($grav_url, array('class' => 'img-rounded')) ?> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<?php $uid = Auth::get_user_id() ?>
						<li><?php echo Html::anchor(Uri::create('user/view/'.$uid[1]), 'Profile') ?></li>
						<li role="separator" class="divider"></li>
						<li><?php echo Html::anchor(Uri::create('logout'), 'Sign out') ?></li>
					</ul>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container -->
</nav>
