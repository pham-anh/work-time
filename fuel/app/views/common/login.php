<div class="col-md-4 col-md-offset-4">
	<h3 class="text-center">Sign in to Work Time</h3>
<br>

<?php echo Form::open(array("class" => "form-horizontal")); ?>
<?php echo Form::csrf(); ?>

<fieldset>
	<div class="form-group">
		<?php echo Form::label('Username', 'username', array('class' => 'control-label')); ?>

		<?php echo Form::input('username', '', array('class' => 'col-md-4 form-control')); ?>

	</div>
	<div class="form-group">
		<?php echo Form::label('Password', 'password', array('class' => 'control-label')); ?>

		<?php echo Form::password('password', '', array('class' => 'col-md-4 form-control')); ?>

	</div>
	<div class="form-group">
		<label class='control-label'>&nbsp;</label>
		<?php echo Form::submit('login', 'Login', array('class' => 'btn btn-success btn-block')); ?>
	</div>
</fieldset>

<?php echo Form::close(); ?>
</div>