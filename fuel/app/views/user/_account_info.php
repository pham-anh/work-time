<div>
		<div class="input-group">
			<span class="input-group-addon" id="name">
				<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
			</span>

			<?php echo Form::input(
				'username',
				Input::post('username', isset($user) ? $user->username : ''),
				array('class' => 'required form-control',
					'aria-describedby' => 'name',
					'title' => 'Username',
					'placeholder' => 'Username'
				)); ?>

		</div>
		<p class="text-danger"><?php echo (Session::get_flash('username')) ?></p>

		<div class="input-group">
			<span class="input-group-addon" id="password">
				<span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
			</span>

			<?php echo Form::input(
				'password',
				Input::post('password', isset($user) ? $user->password : ''),
				array('class' => 'required form-control',
					'aria-describedby' => 'password',
					'placeholder' => 'Password'
				)); ?>

		</div>
		<p class="text-danger"><?php echo (Session::get_flash('password')) ?></p>

		<div class="input-group">
			<span title="Join date" class="input-group-addon" id="since">
				<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
			</span>

			<?php $date = date('Y-m-d', time() + 604800) ?>
			<?php echo Form::input(
				'since',
				Input::post('since', isset($user) ? $user->since : $date),
				array('title' => 'Join date',
					'type' => 'date',
					'class' => 'required form-control',
					'aria-describedby' => 'since',
					'placeholder' => 'yyyy-mm-dd'
				)); ?>
		</div>
	<p class="text-danger"><?php echo (Session::get_flash('since')) ?></p>

</div>