<div class="input-group">
	<span class="input-group-addon" id="name">
		<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
	</span>

	<?php
	echo Form::input(
		'full_name', Input::post('full_name', isset($user->full_name) ? $user->full_name : ''), array('class' => 'required form-control',
		'aria-describedby' => 'name',
		'title' => 'Full name',
		'placeholder' => 'Full name'
	));
	?>
</div>
<p class="text-danger"><?php echo (Session::get_flash('full_name')) ?></p>

<div class="input-group">
		<span title="Birthday" class="input-group-addon" id="birthday">
			<span class="glyphicon glyphicon-gift" aria-hidden="true"></span>
		</span>

		<?php echo Form::input(
			'birthday',
			Input::post('birthday', isset($user->birthday) ? $user->birthday : ''),
			array('type' => 'date',
				'class' => 'form-control',
				'aria-describedby' => 'birthday',
				'title' => 'Birthday',
				'placeholder' => 'mm/dd/yyyy'
			)); ?>
	</div>
	<p class="text-danger"><?php echo (Session::get_flash('birthday')) ?></p>

<!--		<div class="input-group">
			<span title="Avatar" class="input-group-addon" id="avatar">
				<span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
			</span>

			<?php // echo Form::file('avatar', array('class' => 'form-control', 'aria-describedby' => 'avatar')); ?>
		</div>
		<p class="text-danger"><?php // echo (Session::get_flash('avatar')) ?></p>-->