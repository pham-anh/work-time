<h3 class="text-center">
	<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
	Contact information
</h3>
<br>

<div>
	<div class="input-group">
		<span class="input-group-addon" id="phone_number">
			<span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
		</span>

		<?php echo Form::input(
			'phone_number',
			Input::post('phone_number', isset($user->phone_number) ? $user->phone_number : ''),
			array('type' => 'tel',
				'class' => 'col-md-4 form-control',
				'aria-describedby' => 'phone_number',
				'placeholder' => '0084 123 456 789'
			)); ?>
	</div>
	<p class="text-danger"><?php echo (Session::get_flash('phone_number')) ?></p>

	<div class="input-group">
		<span class="input-group-addon" id="email_address">
			<strong>@</strong>
		</span>

		<?php echo Form::input(
			'email_address',
			Input::post('email_address', isset($user->email_address) ? $user->email_address : ''),
			array('class' => 'col-md-4 form-control',
				'aria-describedby' => 'email_address',
				'placeholder' => 'example@example.com'
			)); ?>
	</div>
	<p class="text-danger"><?php echo (Session::get_flash('email_address')) ?></p>

	<div class="input-group">
		<span title="Current address" class="input-group-addon" id="current_address">
			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
		</span>

		<?php echo Form::textarea(
			'current_address',
			Input::post('current_address', isset($user->current_address) ? $user->current_address : ''),
			array('rows' => '2',
				'class' => 'col-md-4 form-control',
				'aria-describedby' => 'current_address',
				'title' => 'Current address',
				'placeholder' => 'Please be as detailed as possible'
			)); ?>
	</div>
	<p class="text-danger"><?php echo (Session::get_flash('current_address')) ?></p>

	<p class="text-center">
	<i><span class="glyphicon glyphicon-fire" aria-hidden="true"></span> In case of emergency, notify</i>
</p>

<div>
	<div class="input-group">
		<span title="Relationship" class="input-group-addon" id="relationship">
			<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
		</span>

		<?php echo Form::input(
			'relationship',
			Input::post('relationship', isset($user->relationship) ? $user->relationship : ''),
			array('class' => 'form-control',
				'aria-describedby' => 'relationship',
				'title' => 'Relationship',
				'placeholder' => 'Mother'
			)); ?>

		<?php echo Form::input(
			'contact_name',
			Input::post('contact_name', isset($user->contact_name) ? $user->contact_name : ''),
			array('class' => 'form-control',
				'aria-describedby' => 'relationship',
				'title' => 'Name',
				'placeholder' => 'Name'
			)); ?>
	</div>
	<p class="text-danger"><?php echo (Session::get_flash('relationship')) ?></p>
	<p class="text-danger"><?php echo (Session::get_flash('contact_name')) ?></p>

	<div class="input-group">
		<span class="input-group-addon" id="urgency_number">
			<span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
		</span>

		<?php echo Form::input(
			'urgency_number',
			Input::post('urgency_number', isset($user->urgency_number) ? $user->urgency_number : ''),
			array('type' => 'tel',
				'class' => 'form-control',
				'aria-describedby' => 'urgency_number',
				'placeholder' => '0084 987 654 321'
			)); ?>
	</div>
	<p class="text-danger"><?php echo (Session::get_flash('urgency_number')) ?></p>
</div>
</div>