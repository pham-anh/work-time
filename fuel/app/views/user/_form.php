<?php echo Form::open(array("class" => "form-horizontal", "enctype" => "multipart/form-data")); ?>
<?php echo Form::csrf(); ?>

<fieldset>

<?php if (Request::active()->action == 'register'): ?>
	<div class="col-md-6 col-md-offset-3">
		<?php echo render('user/_account_info') ?>
	</div>
<?php endif; ?>

<?php if (Request::active()->action == 'update'): ?>
	<div class="col-md-6 col-md-offset-3">
		<?php echo render('user/_personal_info') ?>
	</div>
	<div class="col-md-6 col-md-offset-3">
		<?php echo render('user/_contact_info') ?>
	</div>
<?php endif; ?>

	<div class="col-md-6 col-md-offset-3">
		<hr>
		<div class="col-md-4 pull-right">
			<?php echo Form::submit('submit','Save',array('class' => 'btn btn-primary btn-block')); ?>
		</div>
		<div class="col-md-4">
			<?php echo Html::anchor('user','Cancel',array('class' => 'btn btn-warning btn-block')); ?>
		</div>
	</div>

</fieldset>
<?php echo Form::close(); ?>