<!DOCTYPE html>
<html>
<?php echo render('layout/head', array('title' => $title)); ?>
<body>
	<!--Navigator-->
	<?php if (Request::active()->action != 'login'): ?>
		<?php echo render('layout/navigator'); ?>
	<?php else: ?>
		<?php echo render('layout/banner'); ?>
	<?php endif; ?>

	<!--The content-->
	<div class="container">
		<?php echo render('layout/get_flash'); ?>
		<?php if (Request::active()->action != 'login'): ?>
			<h1><?php echo $title; ?></h1>
			<hr>
		<?php endif; ?>

		<div class="col-md-12"><?php echo $content; ?></div>
	</div>
	<hr>

	<!--Footer-->
	<div class="col-md-12"><?php echo render('layout/footer'); ?></div>

</body>
</html>
