<?php
return array(
	'_root_'  => '',  // The default route
	'_404_'   => '',    // The main 404 route
	'login'   => 'common/login',
	'logout'  => 'common/logout',
);
